#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double ddx_atan(double x) {
	return 1./(x*x +1);
}
double newtown_iterate_arctan(double x0) {
	double x1;
	x1 = x0 - atan(x0)/ddx_atan(x0);
	return x1;
}
void newton_wrapper(double x, double tol) {
	while(abs(atan(x))>tol){
		cout << x << endl;
		x = newtown_iterate_arctan(x);
	}
	cout << x << endl;
	return;
}
void gedampft_newton(double x0, double tau, double tol) {
	double x1;
	double p; //suchrichtung
	double t; //schrittweite
	while(abs(atan(x0))>tol){
		cout << x0 << endl;
		p = (-1.)*atan(x0)/ddx_atan(x0);
		t = 1.;
		x1 = x0 + p;
		while(abs(atan(x1)) >= (1.-tau*t)*abs(atan(x0))){
			t/=2;
			x1 = x0 + t*p;
		}
		x0 = x1;
	}
	cout << x0 << endl;
	return;
}
int main(int argc, char* argv[]) {
	double tol = 10e-16;
	cout << "=== Newtonverfahren für arctan(x), x0=0.5" << endl;
	newton_wrapper(.5, tol);
	cout << "=== Newtonverfahren für arctan(x), x0=1.5" << endl;
	newton_wrapper(1.5, tol);
	cout << "=== gedämpftes Newtonverfahren für arctan(x), x0=1.5, tau=0.5" << endl;
	gedampft_newton(1.5, .5, tol);
	cout << "=== gedämpftes Newtonverfahren für arctan(x), x0=1.5, tau=0.9" << endl;
	gedampft_newton(1.5, .9999999, tol);
	return 0;
}

