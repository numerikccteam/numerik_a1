CC=g++
CFLAGS=-Wall -Wextra -Wshadow -Wconversion
LDFLAGS=
TARGET=test
ARGS=
SOURCE=main.cpp
$(TARGET) : $(SOURCE) #matrix.h eigenvalue.h lu_solver.h
	$(CC) $(CFLAGS) $(SOURCE) $(LDFLAGS) -o $(TARGET)
	./$(TARGET) $(ARGS)

clean:
	rm -f *.o
	rm -f $(TARGET)

run:
	./$(TARGET) $(ARGS)

valgrind:
	valgrind ./$(TARGET) $(ARGS)

